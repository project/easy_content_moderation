<?php

namespace Drupal\easy_content_moderation;

class ModerationEmail
{

  /**
   * @param string
   */
  private $fromEmail;

  /**
   * @param string
   */
  private $fromName;

  /**
   * @param string
   */
  private $url;

  /**
   * @param string
   */
  private $author;

  /**
   * @param string
   */
  private $createdDate;

  /**
   * @param string
   */
  private $title;

  /**
   * @param string
   */
  private $rawBody;

  /**
   * @param string
   */
  private $rawSubject;

  /**
   * @param string
   */
  private $langcode;

  /**
   * @param mixed
   */
  private $config;

  /**
   * @param array
   */
  private $params;

  /**
   * @param int
   */
  private $days_since_revision;

  /**
   * @param mixed
   */
  private $node;

  /**
   * @param string
   */
  private $moderationState;

  /**
   * Construct!
   *
   * @@param mixed $node
   * @param string $rawBody
   * @param string $rawSubject
   */
  public function __construct($node, string $rawBody, string $rawSubject)
  {
    $this->fromEmail = \Drupal::config('system.site')->get('mail');
    $this->fromName = \Drupal::config('system.site')->get('name');
    $this->config = \Drupal::config('easy_content_moderation.settings');

    //ensure that formatted body values still work
    if (is_array($rawBody)) {
      $rawBody = $rawBody['value'];
    }
    $this->rawBody = $rawBody;
    $this->rawSubject = $rawSubject;

    $this->url = $node->toUrl()->setAbsolute()->toString();
    $this->author = $node->getOwner()->getAccountName();
    $this->createdDate = $node->getCreatedTime();
    $this->title = $node->getTitle();
    $this->langcode = \Drupal::currentUser()->getPreferredLangcode();
    $this->node = $node;
    $this->moderationState = null;
  }

  /**
   * Parses and assigns email data
   *
   */
  public function parseData()
  {

    // Parse the node/author data into the email template
    $params['body'] = (string)t($this->rawBody, [
      '@url' => $this->url,
      '@author' => $this->author,
      '@title' => $this->title,
      '@created_date' => date('F j, Y', $this->createdDate)
    ]);

    $params['subject'] = (string)t($this->rawSubject, [
      '@url' => $this->url,
      '@author' => $this->author,
      '@title' => $this->title,
      '@created_date' => date('F j, Y', $this->createdDate)
    ]);

    // parse the node and compute the revision times/etcv
    $this->params = $params;
    $revision_id = $this->node->getRevisionId();
    $revision_storage = \Drupal::service('entity_type.manager')->getStorage('node');
    $revision = $revision_storage->loadRevision($revision_id);
    $revision_time = $revision->getRevisionCreationTime();
    $current_time = time();

    $this->days_since_revision = floor(($current_time - $revision_time) / (60 * 60 * 24));
    if ($revision && $revision->hasField('moderation_state')) {
      $this->moderationState = $revision->get('moderation_state')->value;
    }
  }

  /**
   * returns the raw body
   *
   * @return string
   */
  public function getRawBody()
  {
    return $this->rawBody;
  }

  /**
   * returns the raw subject
   *
   * @return string
   */
  public function getRawSubject()
  {
    return $this->rawSubject;
  }

  /**
   * returns the email parameters
   * 
   * @return array
   */
  public function getParams()
  {
    return $this->params;
  }

  /**
   * returns the user lang code
   * 
   * @return string
   */
  public function getLangCode()
  {
    return $this->langcode;
  }

  /**
   * returns the moderation state
   * 
   * @return string
   */
  public function getModerationState()
  {
    return $this->moderationState;
  }

  /**
   * returns the days since revision
   * 
   * @return int
   */
  public function getDaysSinceRevision()
  {
    return $this->days_since_revision;
  }
}
