<?php

namespace Drupal\easy_content_moderation\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for easy_content_moderation routes.
 */
class EasyContentModerationController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
