<?php

namespace Drupal\easy_content_moderation;

use Drupal\node\NodeInterface;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class ContentModerator
{



  /**
   * @param \mixed
   */
  private $mailManager;

  /**
   * @param string
   */
  private $module;

  /**
   * @param mixed
   */
  private $config;

  /**
   * @param boolean
   */
  private $debug;

  /**
   * @param string
   */
  private $rawBody;

  /**
   * @param string
   */
  private $rawSubject;

  /**
   * @param boolean
   */
  private $send;

  /**
   * @param mixed
   */
  private $nodeBundlesToProcess;

  /**
   * @param array|string
   */
  private $moderators;

  /**
   * @param boolean
   */
  private $sendForApproval;

  /**
   * @param int
   */
  private $delay;

  /**
   * @param mixed
   */
  private $lastRun;

  /**
   * @param string
   */
  private $error;

  /**
   * @param mixed
   */
  private $interval;

  /**
   * @param array
   */
  private $result;

  /**
   * __construct the class
   */
  public function __construct()
  {
    $this->config = \Drupal::config('easy_content_moderation.settings');
    $this->send = true;
    $this->mailManager = \Drupal::service('plugin.manager.mail');
    $this->module = 'easy_content_moderation';
    $this->debug = $this->config->get('moderation_debug') ?? 0;
    $this->nodeBundlesToProcess = $this->config->get('entity_types');
    $this->sendForApproval = false;
    $this->error = '';
    $this->delay = $this->config->get('moderation_email_reminder_days');
    $this->lastRun =  \Drupal::state()->get('easy_content_moderation_last_cron_run', 0);

    // Set variables if this is a debug instance
    if ($this->debug) {
      $this->sendForApproval = true;
      $this->lastRun = 0;
      $this->delay = 1;
    }

    $this->setModerators();
    $this->interval =  $this->delay * 24 * 60 * 60;

    /**
     * Grabs and sets the subject and body of the email
     */
    $this->rawBody = $this->config->get('moderation_email');
    if (is_array($this->rawBody)) {
      $this->rawBody = $this->rawBody['value'];
    }
    $this->rawSubject = $this->config->get('moderation_email_subject');
  }

  /**
   * decides if the cron should continue. If the delay value is >= than 1 AND its been at least a day since the last cron run
   *
   * @return bool
   */
  public function canProcessCron()
  {
    return (!empty($this->delay) && is_numeric($this->delay) && ($this->delay >= 1) && (\Drupal::time()->getRequestTime() - $this->lastRun >= $this->interval));
  }


  /**
   * decides if the cron should continue to process the entity. If the delay value is >= than 1 AND its been [$delay] # of days since the revision, send
   * 
   * @param mixed
   * @param ModerationEmail
   *
   * @return bool
   */
  public function canProcessCronEntity($node, ModerationEmail $email)
  {
    return ($this->canProcess($node) && $this->getDaysSinceRevision($email) == $this->delay);
  }

  /**
   * Returns the days since the last node revision
   * 
   * @return mixed
   */
  public function getDaysSinceRevision(ModerationEmail $email)
  {

    if ($this->debug) {
      return 1;
    } else {
      $email->getDaysSinceRevision();
    }
  }

  /**
   * decides if the current user can process and continue
   *
   * @return bool
   */
  public function canProcess($entity)
  {
    return (in_array($entity->bundle(), $this->nodeBundlesToProcess) &&
      $entity->moderation_state->value == 'draft' &&
      $entity->moderation_state->value !== null &&
      $entity instanceof NodeInterface);
  }

  /**
   * decides if the current user can moderate
   *
   * @return bool
   */
  public function shouldCurrentUserModerate()
  {
    $author = \Drupal::currentUser()->getEmail();
    return strpos($this->moderators, $author) !== null;
  }


  /**
   * sets the moderators
   *
   */
  public function setModerators()
  {
    $this->moderators = $this->config->get('moderators');
  }

  /**
   * Sends the actual email
   *
   * @@param ModerationEmail
   */
  public function sendEmail(ModerationEmail $moderationEmail)
  {
    $params = $moderationEmail->getParams();

    if (!empty($params) && !empty($params['subject']) && !empty($params['body']) && !empty($this->moderators) && $this->sendForApproval) {
      $this->result = $this->mailManager->mail($this->module, 'easy_content_moderation', $this->moderators, $moderationEmail->getLangCode(), $params, NULL, $this->send);
    } else {
      $this->result['result'] = false;
      $this->error = 'There was a problem sending your email and it was not sent.';
    }

    $this->logErrors();
  }


  /**
   * set the cron's last run
   *
   */
  public function setLastRun()
  {
    \Drupal::state()->set('easy_content_moderation_last_cron_run', \Drupal::time()->getRequestTime());
    $this->lastRun = \Drupal::time()->getRequestTime();
  }


  /**
   * Logs any errors
   *
   */
  private function logErrors()
  {
    if (!empty($this->error)) {
      \Drupal::messenger()->addError($this->error);
    }
  }

  /**
   * returns the entities to run the cron on
   *
   * @return mixed
   */
  public function getCronEntities()
  {
    $query = \Drupal::entityQuery('node')
      ->condition('type', $this->nodeBundlesToProcess, 'IN')
      ->condition('status', NODE::NOT_PUBLISHED)
      ->accessCheck(false);

    $nids = $query->execute();
    return Node::loadMultiple($nids);
  }

  /**
   * returns the raw body
   *
   * @return string
   */
  public function getRawBody()
  {
    return $this->rawBody;
  }

  /**
   * returns the raw subject
   *
   * @return string
   */
  public function getRawSubject()
  {
    return $this->rawSubject;
  }
}
