<?php

namespace Drupal\easy_content_moderation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
/**
 * Configure easy_content_moderation settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  private string $tokens = '@url, @author, @title, and @created_date';

/** 
  * The entity type bundle info service.
  *
  * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
  */
 protected $entityTypeBundleInfo;
  /**
   * Constructs a new MyEntitySelectForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;

  }


   /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'easy_content_moderation_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['easy_content_moderation.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = [];
    $bundles = $this->entityTypeBundleInfo->getBundleInfo('node');
    foreach ($bundles as $bundle_id => $bundle_info) {
      $options[$bundle_id] = $bundle_info['label'];
    }

    $defaultBody = $this->config('easy_content_moderation.settings')->get('moderation_email');
    if (is_array($defaultBody)){
      $defaultBody = $defaultBody['value'];
    }

    $form['entity_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select an entity type(s) to moderate'),
      '#options' => $options,
      '#default_value' => $this->config('easy_content_moderation.settings')->get('entity_types')??[],
    ];

    $form['moderators'] = [
      '#type' => 'textfield',
      '#description' => t('Enter super admin emails, comma separated'),
      '#title' => $this->t('News Moderators'),
      '#default_value' => $this->config('easy_content_moderation.settings')->get('moderators'),
      '#placeholder' => 'email@usbr.gov,example-email@usbr.gov'
    ];
    $form['moderation_email_subject'] = [
      '#type' => 'textfield',
      '#description' => t('Set the email subject. Available tokens are ' . $this->tokens),
      '#title' => $this->t('Moderation Alert Email Subject'),
      '#placeholder' => '@author has content to moderate',
      '#default_value' => $this->config('easy_content_moderation.settings')->get('moderation_email_subject'),
    ];
    $form['moderation_email'] = [
      '#type' => 'textarea',
      '#description' => t('Moderation email to send to Super Admins. Available tokens are ' . $this->tokens),
      '#title' => $this->t('Moderation Alert Email Body'),
      '#default_value' => $defaultBody,
      '#placeholder' => 'Hello. @author just created node @title at @created_date. Please moderate and approve at @url.',

    ];
    $form['moderation_email_reminder_days'] = [
      '#type' => 'textfield',
      '#description' => t('Set the # of days for a reminder email. Set as 0 for no reminder email. Reminders will be sent when X days has passed.'),
      '#title' => $this->t('Reminder email delay'),
      '#default_value' => $this->config('easy_content_moderation.settings')->get('moderation_email_reminder_days')??0,
    ];
    $form['moderation_debug'] = [
      '#type' => 'checkbox',
      '#description' => t('Ignore delay restrictions (this will generate emails for all drafted content)'),
      '#title' => $this->t('Enable Debugging'),
      '#default_value' => $this->config('easy_content_moderation.settings')->get('moderation_debug')??0,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('moderators') == '') {
      $form_state->setErrorByName('moderators', $this->t('The value is not correct.'));
    }

    if ($form_state->getValue('moderation_email') == '') {
      $form_state->setErrorByName('moderation_email', $this->t('The value is not correct.'));
    }
    if ($form_state->getValue('moderation_email_subject') == '') {
      $form_state->setErrorByName('moderation_email_subject', $this->t('The value is not correct.'));
    }
    if (!is_numeric($form_state->getValue('moderation_email_reminder_days'))) {
      $form_state->setErrorByName('moderation_email_reminder_days', $this->t('Only numbers may be added.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('easy_content_moderation.settings')
      ->set('entity_types', $form_state->getValue('entity_types'))
      ->set('moderators', $form_state->getValue('moderators'))
      ->set('moderation_email', $form_state->getValue('moderation_email'))
      ->set('moderation_email_subject', $form_state->getValue('moderation_email_subject'))
      ->set('moderation_email_reminder_days', $form_state->getValue('moderation_email_reminder_days'))
      ->set('moderation_debug', $form_state->getValue('moderation_debug'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
