<?php

use Drupal\content_moderation\Plugin\WorkflowType\ContentModeration;
use Drupal\easy_content_moderation\ModerationEmail;
use Drupal\node\NodeInterface;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\easy_content_moderation\ContentModerator;


/**
 * Implements hook_ENTITY_TYPE_update().
 * @param Drupal\Core\Entity\EntityInterface 
 */
function easy_content_moderation_node_update(Drupal\Core\Entity\EntityInterface $entity)
{
  $moderator = new ContentModerator();

  if ($moderator->canProcess($entity)) {
    $mail = new ModerationEmail($entity, $moderator->getRawBody(), $moderator->getRawSubject());
    $mail->parseData();
    $moderator->sendEmail($mail);
  }
}

/**
 * mail HOOK to assign variables
 * @param string
 * @param string
 * @param array
 */
function easy_content_moderation_mail($key, &$message, $params)
{

  switch ($key) {
    case 'easy_content_moderation':
      $message['subject'] = $params['subject'];
      $message['body'] = [];
      $message['body'][] = $params['body'];
      $message['from'] = \Drupal::config('system.site')->get('mail');
      $message['headers'] =  [
        'from' => Drupal::config('system.site')->get('name') . ' <' . \Drupal::config('system.site')->get('mail') . '>',
      ];
  }
}

/**
 * Runs the cron job for reminder emails
 */
function easy_content_moderation_cron()
{
  $moderator = new ContentModerator();

  if ($moderator->canProcessCron()) {

    foreach ($moderator->getCronEntities() as $node) {
      $mail = new ModerationEmail($node, $moderator->getRawBody(), $moderator->getRawSubject());
      $mail->parseData();

      if ($moderator->canProcessCronEntity($node, $mail)) {
        $moderator->sendEmail($mail);
      }
    }
  }
  $moderator->setLastRun();
}
